/**
 * Created by Nicholas on 5/22/14.
 */
'use strict';

angular.module('flagQuiz',['ionic','stateFlagQuiz.controllers','stateFlagQuiz.services','famous.angular'])

.config(function($stateProvider,$urlRouterProvider) {



    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('index',{
            url: '/',
            templateUrl: 'home.html'
        })
        .state('play',{
            url: '/play',
            templateUrl: 'play.html'
        })
        .state('settings',{
            url:'/settings',
            templateUrl: 'settings.html'
        })
        .state('scores',{
            url:'/scores',
            templateUrl: 'score.html'
        })
        .state('answers',{
            url:'/settings/answers',
            templateUrl: 'answers.html'
        });


})

.run(function($rootScope,$ionicPlatform,unFlags,$ionicSideMenuDelegate,$ionicHistory,$state,$ionicModal){

        //mpoints integration
        document.addEventListener("deviceready",function() {

            sessionm.phonegap.startSession('4aa7e46469ec10f2e2d514ddab0f9939d6f38036');

            sessionm.phonegap.listenDidPresentActivity(function(data1){

                sessionm.phonegap.getUnclaimedAchievementCount(function callback(data) {

                    $rootScope.achieveLeft = data.unclaimedAchievementCount;
                });
            });

            $rootScope.recordThing = function(){

                sessionm.phonegap.logAction('Play');
            };



            $rootScope.showPortal = function(){
                sessionm.phonegap.presentActivity('PORTAL');
            };
        }, false);


        //clay.io leaderboard setup
        Clay.ready( function() {

            $rootScope.leaderboard = new Clay.Leaderboard( { id: 4950 } );
            //$rootScope.go=true;

            //leaderboard modal setup
            $ionicModal.fromTemplateUrl('leaderboard.html', {
                scope: $rootScope,
                animation: 'slide-in-up'

            }).then(function(modal) {

                $rootScope.modal = modal;
            });
            //fetch leaderboard info and show modal
            $rootScope.show=function(){
                $rootScope.leaderboard.fetch({limit:10},function(results){

                    $rootScope.highScores=results;
                    //console.log($rootScope.highScores);
                    $rootScope.modal.show();
                });
            };

        });

        //kiip set up
        $rootScope.kiipInstance = new Kiip('6f125712b2bd6f8e198f9c497e2edc30');
        $rootScope.kiipInstance.setTestMode();

        //achievement modal setup
        $ionicModal.fromTemplateUrl('achievements.html', {
            scope: $rootScope,
            animation: 'slide-in-up'

        }).then(function(aModal) {

            $rootScope.achieveModal = aModal;
        });

        //achievement setup
        $rootScope.check1=false;
        $rootScope.check2=false;
        $rootScope.check3=false;
        $rootScope.check4=false;
        $rootScope.check5=false;
        $rootScope.check6=false;
        $rootScope.check7=false;

        //sound setup
        $rootScope.goodSound = new Howl({
            urls: ['sounds/A-Tone-His_Self-1266414414.wav']
        });
        $rootScope.wrongSound = new Howl({
            urls: ['sounds/alarm.wav']
        });
//        ionic.Platform.ready(function(){
//            ionic.Platform.fullScreen();
////            $rootScope.sound=new Media("/android_asset/www/sounds/A-Tone-His_Self-1266414414.mp3");
////            $rootScope.soundTrack= new Media("/android_asset/www/music/Chris_Zabriskie_-_03_-_CGI_Snake.mp3",onSuccess,onError,onStatus);
//            $rootScope.correctSound= new Media("/android_asset/www/sounds/A-Tone-His_Self-1266414414.wav");
//            $rootScope.errorSound= new Media("/android_asset/www/sounds/alarm.wav");
//            $rootScope.soundTrack.play();
//            // onSuccess Callback
//            function onSuccess() {
//            }
//            // onError Callback
//            function onError(error) {
//            }
//            // onStatus Callback
//            function onStatus(status) {
//                if( status==Media.MEDIA_STOPPED ) {
//                    $rootScope.soundTrack.play();
//                }
//            }
//            // background app and pause handler
//            document.addEventListener("pause",onPause,false);
//            function onPause(){
//                //stop timer
//
//                //pause music
//                $rootScope.soundTrack.pause();
//            }
//            //resume app handler
//            document.addEventListener("resume", onResume, false);
//            function onResume(){
//                $rootScope.soundTrack.play();
//            }
//        });
        $rootScope.flags=unFlags;
        $rootScope.randomizeFlags=function(){
            function randOrd(){
                return (Math.round(Math.random())-0.5);
            }
            $rootScope.flags.sort(randOrd);
            var numFlags=$rootScope.flags.length;
            while(numFlags--){
                $rootScope.flags[numFlags].Answers=[];
                $rootScope.flags[numFlags].Answers.push($rootScope.flags[numFlags].id);
                var counter=3;
                var getOut=0;
                while(counter--){
                    if(getOut==3){
                        break;
                    }
                    var num=$rootScope.flags[Math.floor((Math.random() * $rootScope.flags.length) + 0)].id;
                    if($rootScope.flags[numFlags].Answers.indexOf(num)==-1){
//                    console.log(num);
                        $rootScope.flags[numFlags].Answers.push(num);
                        getOut++;
                    }else{
                        counter++;
                    }
                }
//            for(var count=0;count<3;count++){
//                state.Answers.push(unFlags[Math.floor((Math.random() * unFlags.length) + 0)].id);
//            }
                $rootScope.flags[numFlags].Answers.sort(randOrd);
//                console.log(unFlags[numFlags].Answers);
            }
        };
        $rootScope.randomizeFlags();

//        $rootScope.sound1 = new buzz.sound("sounds/A-Tone-His_Self-1266414414", {
//            formats: [ "mp3","wav"],
//            preload: true,
//            autoplay: false,
//            loop: false
//        });
//        $rootScope.sound2 = new buzz.sound("sounds/alarm", {
//            formats: ["wav","mp3"],
//            preload: true,
//            autoplay: false,
//            loop: false
//        });


        //feedback setup
        $rootScope.feedBack={
            //sound feedback
            soundStatus:"ON",
            //vibration feedback
            vibrationStatus:"ON"
        };

//        $rootScope.soundStatus="ON";
//
//        $rootScope.vibrationStatus="ON";

//        unFlags.forEach(function(state){
//
//            state.Answers.push(state.id);
//            var counter=3;
//
//            while(counter--){
//                var num=unFlags[Math.floor((Math.random() * unFlags.length) + 0)].id;
//                if(state.Answers.indexOf(num)==-1){
//                    console.log(num);
//                    state.Answers.push(num);
//                }
//            }
////            for(var count=0;count<3;count++){
////                state.Answers.push(unFlags[Math.floor((Math.random() * unFlags.length) + 0)].id);
////            }
//            state.Answers.sort(randOrd);
//            console.log(state.Answers);
//        });
//        $rootScope.remoteData={
//          url:"",
//          feedData:"",
//          productLink:""
//        };

//        $rootScope.show = function() {
//            $cordovaSpinnerDialog.hide();
//            $ionicLoading.show({
//                template: 'Ready?'
//            });
////            console.log("whoa");
//        };
//        $rootScope.hide = function(){
//            $ionicLoading.hide();
//        };
//        $rootScope.spin=function(){
//
//            $cordovaSpinnerDialog.show();
//        };
//        $rootScope.stopSpin=function(){
//            $cordovaSpinnerDialog.hide();
//        };
        $rootScope.toggleLeftSideMenu = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };
        //$rootScope.goIndex = function(){
        //    $ionicHistory.nextViewOptions({
        //        disableAnimate: true,
        //        disableBack: true,
        //        historyRoot: true
        //    });
        //    $ionicHistory.clearHistory();
        //    $state.go('index');
        //};
        $rootScope.deviceWidth=screen.width;
        $rootScope.deviceHeight=screen.height*.90;
    });