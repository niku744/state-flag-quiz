/**
 * Created by Nicholas on 5/22/14.
 */
'use strict';

var stateFlagQuiz = angular.module('stateFlagQuiz.controllers',['ionic','stateFlagQuiz.services','ngSanitize']);

stateFlagQuiz.controller('homeCtrl',['$rootScope','$scope','$ionicModal','$location','$ionicHistory','unFlags','$state','$ionicLoading','$document','$ionicSideMenuDelegate','$ionicNavBarDelegate',
    function($rootScope,$scope,$ionicModal,$location,$ionicHistory,unFlags,$state,$ionicLoading,$document,$ionicSideMenuDelegate,$ionicNavBarDelegate){
        $ionicHistory.clearHistory();

        document.getElementById('homeCard').classList.add('animated','fadeInUp');

        //$scope.goPlay=function(){
        //
        //    $ionicHistory.nextViewOptions({
        //        disableAnimate: true,
        //        disableBack: true
        //    });
        //
        //    $state.go('play');
        //};

        $scope.openSettings=function(){

            $state.go('settings');
        };

}]);

stateFlagQuiz.controller('playCtrl',['$rootScope','$scope','$timeout','$location','unFlags','$ionicHistory','$element','$window','$document','$ionicPlatform','$famous','$state','$ionicSlideBoxDelegate',
    function($rootScope,$scope,$timeout,$location,unFlags,$ionicHistory,$element,$window,$document,$ionicPlatform,$famous,$state,$ionicSlideBoxDelegate){

    //var EventHandler = $famous['famous/core/EventHandler'];
    //$scope.myEventHandler = new EventHandler();
    //$scope.dimensions=[];
    //$scope.getDims=function(){
    //    $scope.dimensions.push(screen.width);
    //    $scope.dimensions.push(screen.height*.90);
    //    return $scope.dimensions;
    //};

    //$scope.properHeight = document.getElementsByClassName('list-inset').clientHeight;

    //$ionicPlatform.ready(function(){
    //    // background app and pause handler
    //    document.addEventListener("pause",onPause,false);
    //    function onPause(){
    //        //stop timer
    //        if($scope.keepTime){
    //            $timeout.cancel($scope.keepTime);
    //        }
    //    }
    //    //resume app handler
    //    document.addEventListener("resume", onResume, false);
    //    function onResume(){
    //        //resume timer
    //        $scope.keepTime=$timeout($scope.whenTimeOut,1000);
    //    }
    //});
    //clear navigation history
    //$ionicHistory.clearHistory()
    //render all stuff at once variable
    $scope.showStuff=false;

    //flag count variable
    $rootScope.flagCount=0;

    //streak variable
    $rootScope.streak=0;

    //streak animation variable
    $scope.streakShake="";

    //streak list
    $rootScope.streaks=[];

    //score variable
    $rootScope.score=0;

    //time variable
    $scope.time=80;

    ////time color change
    //var timeItem=document.querySelector('#playTime');
    //    console.log(timeItem);
    //sweep(timeItem,'color','#00FF7F','#FF6347',{duration:$scope.time*1000});
    //answer verification code
    $scope.verifyAnswer=function(e){

      //increase flag count
      $rootScope.flagCount++;

      var element=angular.element(e.srcElement);
//          console.log(element.attr('value'));
//          console.log(element.attr('name'));
//          console.log(element.parent().parent().children().children());
//          console.log(element.attr('name'));
//        console.log(element.parent().parent().children().eq(0).children().eq(0));
//      element.parent().children().eq(1).addClass("animated pulse");

      //incorrect handling
      if(element.parent().parent().children().eq(0).children().eq(2).length===0&&
          element.parent().parent().children().eq(1).children().eq(2).length===0&&
          element.parent().parent().children().eq(2).children().eq(2).length===0&&
          element.parent().parent().children().eq(3).children().eq(2).length===0){
          if(element.attr('value')!=element.attr('name')){
              if($rootScope.feedBack.soundStatus==="ON"&&$rootScope.feedBack.vibrationStatus==="ON") {
                $rootScope.wrongSound.play();
                navigator.vibrate(100);
//                  $ionicPlatform.ready(function () {
////                      var sound1= new Media("/android_asset/www/sounds/A-Tone-His_Self-1266414414.mp3");
////                      sound1.play();
////                      $rootScope.sound.play();
//                      $rootScope.errorSound.play();
//                      $cordovaVibration.vibrate(100);
//
//                  });

                  element.parent().append('<i class="radio-icon ion-close assertive"></i>');
              }else if($rootScope.feedBack.soundStatus!=="ON"&&$rootScope.feedBack.vibrationStatus==="ON") {
                  navigator.vibrate(100);
                  //$ionicPlatform.ready(function () {
                  //    $cordovaVibration.vibrate(100);
                  //});

                  element.parent().append('<i class="radio-icon ion-close assertive"></i>');
              }else if($rootScope.feedBack.soundStatus==="ON"&&$rootScope.feedBack.vibrationStatus!=="ON"){
                  $rootScope.wrongSound.play();
                  //$ionicPlatform.ready(function () {
                  //    $rootScope.errorSound.play();
                  //});

                  element.parent().append('<i class="radio-icon ion-close assertive"></i>');
              }else{
                  element.parent().append('<i class="radio-icon ion-close assertive"></i>');
              }

//              console.log(element.parent().parent().children());
              element.parent().parent().children().eq(0).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(1).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(2).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(3).children().eq(0).prop('disabled',true);

              //end streak count
              $rootScope.streak=$rootScope.streak-$rootScope.streak;
              $scope.streakShake="";
          }else{
              //correct handling

//              console.log($rootScope.sound1);
                //sound for correct answer
              if($rootScope.feedBack.soundStatus==="ON"&&$rootScope.feedBack.vibrationStatus==="ON") {
                  $rootScope.goodSound.play();
                  navigator.vibrate(100);
                  //$ionicPlatform.ready(function () {
                  //    $rootScope.correctSound.play();
                  //    $cordovaVibration.vibrate(100);
                  //});

                  element.parent().append('<i class="radio-icon ion-checkmark balanced"></i>');
              }else if($rootScope.feedBack.soundStatus!=="ON"&&$rootScope.feedBack.vibrationStatus==="ON") {
                  navigator.vibrate(100);
                  //$ionicPlatform.ready(function () {
                  //    $cordovaVibration.vibrate(100);
                  //});

                  element.parent().append('<i class="radio-icon ion-checkmark balanced"></i>');
              }else if($rootScope.feedBack.soundStatus==="ON"&&$rootScope.feedBack.vibrationStatus!=="ON"){
                  $rootScope.goodSound.play();
                  //$ionicPlatform.ready(function () {
                  //    $rootScope.correctSound.play();
                  //});

                  element.parent().append('<i class="radio-icon ion-checkmark balanced"></i>');
              }else{
                  element.parent().append('<i class="radio-icon ion-checkmark balanced"></i>');
              }

//              element.parent().append('<i class="radio-icon ion-checkmark balanced"></i>');
//              console.log(angular.element(document.getElementById(element.attr('value')+1)));
              $rootScope.score++;
              //$document.ready(function(){
              //    var el=document.querySelector('#score');
              //    var od = new Odometer({
              //        el: el,
              //        value: 0,
              //
              //        // Any option (other than auto and selector) can be passed in here
              //        format: 'd',
              //        theme: 'minimal'
              //    });
              //    od.update($rootScope.score);
              //});

//         console.log(name+1);
//          document.getElementById(name+1).style.display="initial";
//          document.getElementById(name+1).style.visibility="initial";
//              console.log(element.parent().parent().children());
              element.parent().parent().children().eq(0).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(1).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(2).children().eq(0).prop('disabled',true);
              element.parent().parent().children().eq(3).children().eq(0).prop('disabled',true);

              //increase streak
              $rootScope.streak++;

              //streak shake checker
              if($rootScope.streak===5){
                  $scope.streakShake="shake shake-little shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===10){
                  $scope.streakShake="shake shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===20){
                  $scope.streakShake="shake shake-horizontal shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===30){
                  $scope.streakShake="shake shake-vertical shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===40){
                  $scope.streakShake="shake shake-slow shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===50){
                  $scope.streakShake="shake shake-rotate shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===60){
                  $scope.streakShake="shake shake-hard shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===70){
                  $scope.streakShake="shake shake-opacity shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak===80){
                  $scope.streakShake="shake shake-crazy shake-constant";
                  console.log($scope.streakShake);
              }else if($rootScope.streak>=90){
                  $scope.streakShake="shake shake-constant shake-constant";
                  console.log($scope.streakShake);
              }
              if($rootScope.streaks.indexOf($rootScope.streak)!=-1){
                  $rootScope.streaks.push($rootScope.streak);
              }

          }
      }
        $timeout(function(){
            //console.log($famous.find('fa-scroll-view')[0].renderNode);
            $famous.find('fa-scroll-view')[0].renderNode.goToNextPage();
            //console.log($famous.find('fa-scroll-view')[0].renderNode.getCurrentIndex());
            //$ionicSlideBoxDelegate.next();
        },100,false);
    };

      function doneRendering(){

          $scope.showStuff=true;

          //time color change
          var timeItem=document.querySelector('#playTime');
          //console.log(timeItem);
          sweep(timeItem,'color','#00FF7F','#DC143C',{duration:$scope.time*1000});
//          console.log("jo");
//          setTimeout(function(){
              //$rootScope.stopSpin();

              $scope.whenTimeOut=function(){
                  $scope.time--;
                  //var el=document.querySelector('#time');
                  //var od = new Odometer({
                  //    el: el,
                  //    value: 30,
                  //
                  //    // Any option (other than auto and selector) can be passed in here
                  //    format: 'd',
                  //    theme: 'minimal'
                  //});
                  //od.update($scope.time);

                  if($scope.time>0){
                      $scope.keepTime= $timeout($scope.whenTimeOut,1000);
                  }else{
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true
                        });
                        $state.go('scores');
                  }
              };

              $scope.keepTime= $timeout($scope.whenTimeOut,1000);
              $scope.$on('$destroy',function(){
                  console.log("yo");
                  if($scope.keepTime){
                      $timeout.cancel($scope.keepTime);
                  }
              });
          //},3000);
      }

      $document.ready(function(){

          doneRendering()
      });

}]);

stateFlagQuiz.controller('scoresCtrl',['$rootScope','$scope','$location','$ionicHistory','unFlags',function($rootScope,$scope,$location,$ionicHistory,unFlags){

    //score streak
    $rootScope.bestStreak=Math.max.apply( null,$rootScope.streaks);

    //clear history
    $ionicHistory.clearHistory();

    //randomize flags
    $rootScope.randomizeFlags();

    //log mPoints action
    $rootScope.recordThing();

    //post score to leaderboard
    $rootScope.leaderboard.post({score: $rootScope.score});

    //Award Kiip reward based on score
    if($rootScope.score >= 10){
        //( new Clay.Achievement( { id: 5472 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 10!');
    }
    if($rootScope.score >= 20){
        //( new Clay.Achievement( { id: 5473 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 20!');
    }
    if($rootScope.score >= 30){
        //( new Clay.Achievement( { id: 5474 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 30!');
    }
    if($rootScope.score >= 40){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 40!');
    }
    if($rootScope.score >= 50){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 50!');
    }
    if($rootScope.score >= 150){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 150!');
    }
    if($rootScope.score >= 193){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a high score of 193!');
    }

    //Award Kiip reward based on streak
    if($rootScope.streak>=10){
        //( new Clay.Achievement( { id: 5472 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 10!');
    }
    if($rootScope.streak>=20){
        //( new Clay.Achievement( { id: 5473 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 20!');
    }
    if($rootScope.streak>=30){
        //( new Clay.Achievement( { id: 5474 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 30!');
    }
    if($rootScope.streak>=40){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 40!');
    }
    if($rootScope.streak>=50){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 50!');
    }
    if($rootScope.streak>=150){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 150!');
    }
    if($rootScope.streak>=193){
        //( new Clay.Achievement( { id: 5475 } ) ).award();
        $rootScope.kiipInstance.postMoment('Getting a streak of 193!');
    }

}]);

stateFlagQuiz.controller('settingsCtrl',['$rootScope','$scope','$location','$ionicHistory',function($rootScope,$scope,$location,$ionicHistory){
    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });
    $ionicHistory.clearHistory();
    //$rootScope.soundStatus="ON";
    //
    //$rootScope.vibrationStatus="ON";
}]);

stateFlagQuiz.controller('answersCtrl',['$rootScope','$scope','$location','$http','$element','$state','$sce',function($rootScope,$scope,$location,$http,$element,$state,$sce){

    $rootScope.flags.sort(function(a,b){
        var name1= a.id;
        var name2= b.id;

        if(name1<name2){
            return -1;
        }

        if(name1>name2){
            return 1;
        }

        return 0;
    });

}]);